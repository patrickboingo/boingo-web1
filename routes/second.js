var express = require("express");

var second = express.Router();

second.use(function (req, res, next) {
	res.render("second", {
		message: "This is the second"
	});
});

module.exports = second;