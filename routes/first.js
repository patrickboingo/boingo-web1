var express = require("express");

var first = express.Router();

first.use(function (req, res, next) {
	res.render("first", {
		message: "This is the first"
	});
});

module.exports = first;