var express = require("express");

var index = express.Router();

index.use(function (req, res, next) {
	res.render("index", {
		message: "This is the index"
	});
});

module.exports = index;