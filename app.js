var express = require("express");
var path = require("path");
var http = require("http");
var logger = require("morgan");
var fs = require("fs");

var app = express();

var indexRouter = require("./routes/index");
var firstRouter = require("./routes/first");
var secondRouter = require("./routes/second");

app.use(logger("dev")); // combined for longer output.

var publicPath = path.resolve(__dirname, "public");

app.set("views", path.resolve(__dirname, "views"));
app.set("view engine", "ejs");

// app.use(function(req, res, next) {
// 	console.log("Request IP: "+ req.ip);
// 	console.log("Request URL: " + req.url);
// 	console.log("Request date: " + new Date());
// 	next();
// });

app.use(express.static(publicPath));

// app.use(function(req, res, next) {
// 	var filePath = path.join(publicPath, req.url);
// 	fs.stat(filePath, function(err, fileInfo) {
// 		if (err) {
// 			next();
// 			return;
// 		}
// 		if (fileInfo.isFile()) {
// 			res.sendFile(filePath);
// 		} else {
// 			next();
// 		}
// 	}); 
// });


app.get('/', indexRouter);

app.get('/first', firstRouter);

app.get('/second', secondRouter);

// app.use(function(req, res) {
// 	res.writeHead(200, { "Content-Type": "text/plain"});
// 	res.end("No file by that name.");
// });


// app.get('', function() {
	
// });

// app.get('', function() {
	
// });

app.use(function(req, res) {
	res.status(404).render("404");
});


http.createServer(app).listen(3000, function() {
	console.log("Server created on port 3000");
});